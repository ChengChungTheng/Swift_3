//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

var firstName: String? = "Mic"
var lastName: String? = "Pringle"



if let firstName = firstName {
  print(firstName)
  if let lastName = lastName {
    print(lastName)
  }
}

//lastName = nil
if let firstName = firstName, let lastName = lastName {
  print("\(firstName) \(lastName)")
}

if var firstName = firstName, var lastName = lastName {
  firstName = "Sam"
  lastName = "Davies"
  print("\(firstName) \(lastName)")
}

func print(firstName: String?, andLastName lastName: String?) {
  if firstName != nil {
    print(firstName!)
  }
  
  guard let firstName = firstName, let lastName = lastName else {
    print("exiting ... ")
    return
  }
  print("\(firstName) \(lastName)")
}

//print(firstName: "Ray", andLastName: "Wenderlich")
print(firstName: nil, andLastName: "Wenderlich")
